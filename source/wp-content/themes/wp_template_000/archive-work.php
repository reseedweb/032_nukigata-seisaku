<?php get_header(); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2-title">抜型製作事例</h2>	
		<?php
	    $queried_object = get_queried_object();
	    //print_r($queried_object);
	    ?>
	    <?php 
			$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
			$posts = get_posts(array(
	        'post_type'=> 'work',
	        'posts_per_page' => 2,
	        'paged' => $paged,
			//'orderby'           => 'slug', 
			//'order'             => 'ASC',  	
	    ));
	    ?>
	<div class="message-group message-work375 message-classic"><!-- begin message-group -->
		<?php $i = 0; ?>
	    <?php foreach($posts as $p) :  ?>
	        <?php $i++; ?>
	        <?php if($i%2 == 1) : ?>	
        <div class="message-row clearfix"><!--message-row -->
		<?php endif; ?>     
           <div class="message-col">
				<h3 class="work-title"><a href="<?php echo get_the_permalink($p->ID); ?>"><?php echo $p->post_title; ?></a></h3>
				<div class="work-img">
					<a href="<?php echo get_the_permalink($p->ID); ?>">
						<?php echo get_the_post_thumbnail( $p->ID,'large'); ?>                        
					</a>
				</div><!-- ./work-image -->  
                <div class="work-text">
					材質名: <?php the_field('material', $p->ID); ?>
				</div><!-- ./work-text -->  				
          </div><!-- end message-col -->          
			 <?php if($i%2 == 0 || $i == count($posts) ) : ?>
       </div><!-- end message-row -->	
		<?php endif; ?>
	    <?php endforeach; ?>		
  </div><!-- end message-group -->
	<div class="primary-row text-center">
	    <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
	</div>
	
	<?php wp_reset_query(); ?>
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->        
    <?php get_template_part('part','flow'); ?>
    <?php get_template_part('part','contact'); ?>
</div><!-- end primary-row -->
<?php get_footer(); ?>