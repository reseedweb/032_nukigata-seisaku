	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<h2 class="h2-title">抜型作成までの流れ</h2>
		<div class="process-row clearfix"><!-- begin process-row -->
			<div class="process-product clearfix">
				<h3 class="process-ptitle">STEP1：お客様からの発注</h3>
				<img src="<?php bloginfo('template_url'); ?>/img/content/process_product_img1.jpg" alt="process" />
				<p class="pt10">商談やメール・FAX・WEBサイトからの受注。</p>
			</div><!-- ./process-product -->
			
			<div class="process-product-arrow">
				<img src="<?php bloginfo('template_url'); ?>/img/content/process_product_arrow.png" alt="process" />
			</div><!-- ./process-product-arrow -->
			
			<div class="process-product clearfix">
				<h3 class="process-ptitle">STEP2：社内打ち合せ</h3>
				<img src="<?php bloginfo('template_url'); ?>/img/content/process_product_img2.jpg" alt="process" />
				<p class="pt10">営業と当社専門スタッフによる綿密な打ち合わせをします。</p>
			</div><!-- ./process-product -->
			
			<div class="process-product-arrow">
				<img src="<?php bloginfo('template_url'); ?>/img/content/process_product_arrow.png" alt="process" />
			</div><!-- ./process-product-arrow -->
			
			<div class="process-product clearfix">
				<h3 class="process-ptitle">STEP3：CADによる図面作成</h3>
				<img src="<?php bloginfo('template_url'); ?>/img/content/process_product_img3.jpg" alt="process" />
				<p class="pt10">展開されたケースをCADにて作図します。</p>
			</div><!-- ./process-product -->
		</div><!-- end process-row -->
	
		<div class="process-row clearfix"><!-- begin process-row -->
			<div class="process-product clearfix">
				<h3 class="process-ptitle">STEP4：抜型作成</h3>
				<img src="<?php bloginfo('template_url'); ?>/img/content/process_product_img4.jpg" alt="process" />
				<p class="pt10">レーザーカット機・刃物自動曲げ機と自動切断機で抜型を作成します。</p>
			</div><!-- ./process-product -->
			
			<div class="process-product-arrow">
				<img src="<?php bloginfo('template_url'); ?>/img/content/process_product_arrow.png" alt="process" />
			</div><!-- ./process-product-arrow -->
			
			<div class="process-product clearfix">
				<h3 class="process-ptitle">STEP5：最終チェックシステム</h3>
				<img src="<?php bloginfo('template_url'); ?>/img/content/process_product_img5.jpg" alt="process" />
				<p class="pt10">弊社独自のチェックシステムで納品前の最後の品質チェックを行います。</p>
			</div><!-- ./process-product -->
			
			<div class="process-product-arrow">
				<img src="<?php bloginfo('template_url'); ?>/img/content/process_product_arrow.png" alt="process" />
			</div><!-- ./process-product-arrow -->
			
			<div class="process-product process-height clearfix">
				<h3 class="process-ptitle">STEP6：抜型納入</h3>
				<img src="<?php bloginfo('template_url'); ?>/img/content/process_product_img6.jpg" alt="process" />
				<p class="pt10">完成した抜型(木型)をお客様へ納品します。</p>
			</div><!-- ./process-product -->
		</div><!-- end process-row -->
	</div><!-- end primary-row -->	
	
		