<?php get_header(); ?>	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2-title">抜型製作.comのスタッフ紹介</h2>  
		<img src="<?php bloginfo('template_url'); ?>/img/content/staff_content_img1.jpg" alt="staff" />		
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="message-left message-220 clearfix"><!-- begin message-252 -->
			<h3 class="h3-title">京桐　淳</h3>
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/staff_content_img2.jpg" alt="staff" />
			</div><!-- ./image -->
			<div class="text ln15em">				
				<table class="staff-info">
					<tr>
						<th>出身</th>
						<td>00年</td>
					</tr>
					<tr>
						<th>趣味</th>
						<td>○○○○○○○○○○○○○○○○</td>
					</tr>
					<tr>
						<th>こだわり</th>
						<td>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</td>
					</tr>
					<tr>
						<th>メッセージ</th>
						<td>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</td>
					</tr>
				</table><!-- ./staff-info -->
			</div><!-- ./text -->
		</div><!-- end message-252 -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="message-left message-220 clearfix"><!-- begin message-252 -->
			<h3 class="h3-title">お名前お名前</h3>
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/staff_content_img3.jpg" alt="staff" />
			</div><!-- ./image -->
			<div class="text ln15em">				
				<table class="staff-info">
					<tr>
						<th>出身</th>
						<td>00年</td>
					</tr>
					<tr>
						<th>趣味</th>
						<td>○○○○○○○○○○○○○○○○</td>
					</tr>
					<tr>
						<th>こだわり</th>
						<td>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</td>
					</tr>
					<tr>
						<th>メッセージ</th>
						<td>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</td>
					</tr>
				</table><!-- ./staff-info -->
			</div><!-- ./text -->
		</div><!-- end message-252 -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="message-left message-220 clearfix"><!-- begin message-252 -->
			<h3 class="h3-title">お名前お名前</h3>
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/staff_content_img4.jpg" alt="staff" />
			</div><!-- ./image -->
			<div class="text ln15em">				
				<table class="staff-info">
					<tr>
						<th>出身</th>
						<td>00年</td>
					</tr>
					<tr>
						<th>趣味</th>
						<td>○○○○○○○○○○○○○○○○</td>
					</tr>
					<tr>
						<th>こだわり</th>
						<td>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</td>
					</tr>
					<tr>
						<th>メッセージ</th>
						<td>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</td>
					</tr>
				</table><!-- ./staff-info -->
			</div><!-- ./text -->
		</div><!-- end message-252 -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<p><img src="<?php bloginfo('template_url'); ?>/img/content/staff_content_img5.jpg" alt="staff" /></p>
		<p class="pt10"><img src="<?php bloginfo('template_url'); ?>/img/content/staff_content_img6.jpg" alt="staff" /></p>
		<p class="pt10"><img src="<?php bloginfo('template_url'); ?>/img/content/staff_content_img7.jpg" alt="staff" /></p>
	</div><!-- end primary-row -->
	
<?php get_template_part('part','contact');?>
<?php get_footer(); ?>