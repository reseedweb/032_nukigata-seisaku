<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- primary-row -->
		<h2 class="h2-title">よくあるご質問</h2>	
		<p>お客様からお問い合わせの多いご質問について、回答を掲載しております。</p>
		<p>下記以外の疑問・お悩みにつきましてはお気軽にお問い合わせください。</p>
		<div class="primary-row clearfix"><!-- primary-row -->
			<div class="faq-content-row clearfix">
				<h3 class="question-bg">
					小ロットとはどのくらいですか？
				</h3><!-- ./uestion-bg -->
				<div class="answer-bg">
					<div class="answer">
						<p>1枚から承っております。</p>
						<p>詳細はお気軽にご相談ください。</p>

					</div>
				</div><!-- ./answer-bg -->	
			</div><!-- ./faq-content-row -->
		</div><!-- ./primary-row -->
	</div><!-- end primary-row -->	
	
	<div class="primary-row clearfix"><!-- primary-row -->
		<div class="faq-content-row clearfix">
			<h3 class="question-bg">
				どんなデータ形式に対応していますか？
			</h3><!-- ./uestion-bg -->
			<div class="answer-bg">
				<div class="answer">
					<p>弊社では以下のデータ形式に対応しています。</p>
					<p>dxf・eps・ai・pdf・その他2次元、3次元のCADデータ等</p>
					<p>上記の他にも、多くのフォーマットに対応しております。</p>
					<p>詳細は、お問合せください。</p>
				</div>
			</div><!-- ./answer-bg -->	
		</div><!-- ./faq-content-row -->
	</div><!-- ./primary-row -->
	
	<div class="primary-row clearfix"><!-- primary-row -->
		<div class="faq-content-row clearfix">
			<h3 class="question-bg">
				試作品の製作は可能でしょうか？
			</h3><!-- ./uestion-bg -->
			<div class="answer-bg">
				<div class="answer" style="height:30px;">
					<p>弊社では、サンプルや試作品の製作も行っていますので、お気軽にご相談下さい。</p>					
				</div>
			</div><!-- ./answer-bg -->	
		</div><!-- ./faq-content-row -->
	</div><!-- ./primary-row -->
	
	<div class="primary-row clearfix"><!-- primary-row -->
		<div class="faq-content-row clearfix">
			<h3 class="question-bg">
				トムソン型とは何でしょうか？
			</h3><!-- ./uestion-bg -->
			<div class="answer-bg">
				<div class="answer">
					<p>トムソン型とは木の板や樹脂版に溝を掘り、その溝の部分に、同じ形に曲げた鋼の刃を埋め込んで作られた木型のことです。</p>
					<p>そのトムソン型を用いて打ち抜き加工をしたものがトムソン加工といわれます。</p>
					<p>従来の金型加工に比べ、コストダウン・スピードアップが可能となり、高精度でありながら、受注から納品までの日数がぐっと短縮されました。</p>
				</div>
			</div><!-- ./answer-bg -->	
		</div><!-- ./faq-content-row -->
	</div><!-- ./primary-row -->
	
	<div class="primary-row clearfix"><!-- primary-row -->
		<div class="faq-content-row clearfix">
			<h3 class="question-bg">
				納期は相談できますか？
			</h3><!-- ./uestion-bg -->
			<div class="answer-bg">
				<div class="answer">
					<p>出来る限り、お客様のご要望にお応えする努力をいたしますので、お気軽にご相談下さい。</p>
					<p><span style="color:#ff0000;">※</span>簡単な形状であれば2・3日に出荷できる場合もございます。</p>
				</div>
			</div><!-- ./answer-bg -->	
		</div><!-- ./faq-content-row -->
	</div><!-- ./primary-row -->
	
	<div class="primary-row clearfix"><!-- primary-row -->
		<div class="faq-content-row clearfix">
			<h3 class="question-bg">
				材料はどのようなものを使いますか？
			</h3><!-- ./uestion-bg -->
			<div class="answer-bg">
				<div class="answer">
					<p>紙厚の薄いものから厚いものまで、素材では紙全般(カード紙、輪転シート、透明紙)ペット素材などを加工する事が出来ます。</p>
					<p>また、レーザー加工ではアクリル、ベイク、樹脂などに加工が可能です。</p>
				</div>
			</div><!-- ./answer-bg -->	
		</div><!-- ./faq-content-row -->
	</div><!-- ./primary-row -->
	<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>