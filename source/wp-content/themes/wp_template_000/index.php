<?php get_header(); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
	<div class="top-content-info1 clearfix"><!-- begin top-content-info1 -->
        <img alt="contact" src="<?php bloginfo('template_url');?>/img/content/top_content_img1.jpg" />
        <div class="top-info1-btn">
            <a href="<?php bloginfo('url'); ?>/work">
                <img alt="contact" src="<?php bloginfo('template_url');?>/img/content/top_content_img1_btn.png" />
            </a>
        </div><!-- ./top-info1_btn -->		
    </div><!-- end top-content-info1 -->
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<div class="top-content-info2 clearfix"><!-- begin top-content-info2 -->
        <img alt="contact" src="<?php bloginfo('template_url');?>/img/content/top_content_img2.jpg" />
        <h2 class="top-info2-title">抜型製作.comが選ばれる４つの理由</h2>
		<div class="top-info2-text1">
			抜型・木型製作で<br />
			こんなお悩みをお持ちの方へご案内
		</div>
		<div class="top-info2-ltext2">
			<ul>
				<li>
					<span class="side-tclr">あらゆる材質</span> に対応できる抜型が欲しい				
				</li>
				<li>
					<span class="side-tclr">試作品</span> を製作したい				
				</li>
				<li>
					経験のある職人に <span class="side-tclr">的確な提案</span> をお願いしたい				
				</li>
				<li>
					<span class="side-tclr">短納期</span> でお願いしたい				
				</li>				
			</ul>
		</div>
    </div><!-- end top-content-info2 -->

	<div class="top-text1">
		<p class="text1-font1">当社は創業20年の歴史があります。</p>
		<p class="text1-font2">創業以来培ってきた実績や経験でお客様のご要望にお応えいたします。</p>
	</div>
	<div class="top-text2">		
		<p class="text2-font1">木型・抜き型なら</p>
		<p class="text2-font2"><span class="top-text2-clr">抜型製作.com</span>にお任せください！</p>
	</div>
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<div class="top-content-info3 clearfix">
		<div class="top-info3-title clearfix">
			<span class="top-info3-title1">短<span style="color:#ff7800;">納</span>期</span>
			<h3 class="top-info3-title2">最短2日～ 全国対応で納品可能</h3>			
		</div><!-- ./top-info3-title -->
		<div class="top-info3-text">	
			<p>当社は創業20年の経験から培った確かな技術をもとに、高品質の抜型・木型を製作します。</p>
			<p>これまで、品質に関するクレームはほぼゼロで、リピート率91%という高さを保っております。</p>
		</div><!-- ./top-info3-text -->
	</div><!-- ./top-content-info3 -->
	
	<div class="top-content-info3 clearfix">
		<div class="top-info3-title clearfix">
			<span class="top-info3-title1"><span style="color:#ff7800;">多</span>品<span style="color:#ff7800;">種</span></span>
			<h3 class="top-info3-title2">多様な機械を所有しているのでどんな抜型でも対応可</h3>
			</p>
		</div><!-- ./top-info3-title -->
		<div class="top-info3-text">	
			<p>トムソン型(オートン・ハイデル・ビク・アップダウン)の抜型はもちろん、小ロット・試作用途のサンプルカット、ベニヤ材・アクリルやベイク-PP-ABSなどの樹脂を加工するレーザーカットを行う設備も所有している為、多様な要望にお応えすることが可能です。</p>
		</div><!-- ./top-info3-text -->
	</div><!-- ./top-content-info3 -->
	
	<div class="top-content-info3 clearfix">
		<div class="top-info3-title clearfix">
			<span class="top-info3-title1">実<span style="color:#ff7800;">績</span><span style="color:#ff7800;">多</span></span>
			<h3 class="top-info3-title2">100,000万版以上の製作実績!</h3>			
		</div><!-- ./top-info3-title -->
		<div class="top-info3-text">	
			<p>抜型・木型の事なら、抜型製作.comにお任せください！！</p>
			<p>創業以来、トムソン抜型を、創意工夫を重ねて作ってまいりました。</p>
			<p>その成果もあり、今まで100,000万版以上の抜型を製作してきました。</p>
		</div><!-- ./top-info3-text -->
	</div><!-- ./top-content-info3 -->
	
	<div class="top-content-info3 clearfix">
		<div class="top-info3-title clearfix">
			<span class="top-info3-title1"><span style="color:#ff7800;">対</span>応<span style="color:#ff7800;">力</span></span>
			<h3 class="top-info3-title2">低価格、短納期などのニーズにあった要望にお応えします! !</h3>
		</div><!-- ./top-info3-title -->
		<div class="top-info3-text">	
			<p>抜型・木型の事なら、抜型製作.comにお任せください！！</p>
			<p>創業以来、トムソン抜型を、創意工夫を重ねて作ってまいりました。</p>
			<p>その成果もあり、今まで100,000万版以上の抜型を製作してきました。</p>
		</div><!-- ./top-info3-text -->
	</div><!-- ./top-content-info3 -->
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
    <h2 class="h2-title">サービス紹介 </h2>
	<div class="top-content-info4 clearfix">
		<div class="top-info4 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/top_content_img3.jpg" alt="top" />
			</div><!-- ./image -->
			<div class="text">
				<h3 class="top-info4-title">抜型・木型製作</h3>
				<p>抜型(木型)とは、私たちの生活の中でとても身近なものに作るのに使われています。</p>
				<p>例えば、ティッシュペーパーの箱、お菓子の箱、医薬品の箱、鍋の箱、電化製品の箱ような、私たちが普段何気なく使っている、所謂パッケージは抜型を使用されています。</p>
				<p>弊社では、お客様のニーズに合った最適な抜型・木型を提案させていただきます。</p>			
				<div class="text-right mt10">
					<a href="<?php bloginfo('url'); ?>/about">					
						<img src="<?php bloginfo('template_url'); ?>/img/content/top_content_btn.jpg" alt="top" />
					</a>
				</div>
			</div><!-- ./text -->
		</div><!-- ./top-info4 -->
	</div><!-- end top-content-info4 -->
	
	<div class="top-content-info4 mt10 clearfix">
		<div class="top-info4 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/top_content_img4.jpg" alt="top" />
			</div><!-- ./image -->
			<div class="text">
				<h3 class="top-info4-title">サンプルカット</h3>
				<p>抜型製作.comでは抜型・木型製造の他、サンプル機による試作品の作成を行っております。</p>
				<p>木型・抜型を作る前に、まずはサンプルカット(有料)をしてほしい、サンプルだけを作って欲しいなどお客様の要望にお応えします。</p>			
				<div class="text-right mt10">
					<a href="<?php bloginfo('url'); ?>/sample">
						<img src="<?php bloginfo('template_url'); ?>/img/content/top_content_btn.jpg" alt="top" />
					</a>
				</div>
			</div><!-- ./text -->
		</div><!-- ./top-info4 -->
	</div><!-- end top-content-info4 -->
	
	<div class="top-content-info4  mt10 clearfix">
		<div class="top-info4 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/top_content_img5.jpg" alt="top" />
			</div><!-- ./image -->
			<div class="text">
				<h3 class="top-info4-title">レーザーカット</h3>
				<p>職人による正確なレーザーカットで、抜型製造に関係なく、ベニヤ材の他、アクリルやベイク-PP-ABSなどの樹脂もレーザーでカットしております。</p>
				<p>また、他社からの各種レーザーカットも随時受注しております。</p>			
				<div class="text-right mt10">
					<a href="<?php bloginfo('url'); ?>/laser">
						<img src="<?php bloginfo('template_url'); ?>/img/content/top_content_btn.jpg" alt="top" />
					</a>
				</div>
			</div><!-- ./text -->
		</div><!-- ./top-info4 -->
	</div><!-- end top-content-info4 -->
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<div class="top-content-info5 clearfix"><!-- begin top-content-info5  -->
        <img alt="top" src="<?php bloginfo('template_url');?>/img/content/top_content_img6.jpg" />
		<div class="top-info5-text">			
			<span class="info5-icon">■</span> 熟練したスタッフが製作<br />
			<span class="info5-icon">■</span> 様々な業界からの要望に対応<br />
			<span class="info5-icon">■</span> 100,000以上の抜型の実績!<br />
			<span class="info5-icon">■</span> 最新の設備を導入
		</div>
        <div class="top-info5-movie">
			<iframe width="360" height="218" src="//www.youtube.com/embed/kl6qaFSkMb4" frameborder="0" allowfullscreen></iframe>
		</div>		
    </div><!-- end top-content-info5 -->
</div><!-- end primary-row -->

<?php get_template_part('part','contact'); ?>

<div class="primary-row clearfix"><!-- begin primary-row -->	
	<div class="top-content-info7 clearfix">
		<img alt="top" src="<?php bloginfo('template_url');?>/img/content/top_content_sample.png" />
		<div class="top-info7-btn">  
			<a href="<?php bloginfo('url'); ?>/sample">
				<img alt="top" src="<?php bloginfo('template_url');?>/img/content/top_sample_btn.jpg" />
			</a>
		</div><!-- ./top-info7-btn -->	
	</div><!-- ./top-content-info7 -->
</div><!-- end primary-row -->

<?php get_footer(); ?>