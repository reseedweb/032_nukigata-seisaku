<?php get_header(); ?>	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2-title">保有設備のご紹介</h2>  
		<div class="message-group message-col375 message-classic"><!-- begin message-group -->
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col">					
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/facility_content_img1.jpg" alt="facility" />
					</div><!-- ./image -->    
					<div class="text text-center">
						レーザーカット機
					</div><!-- ./text -->         
				</div><!-- end message-col -->
				<div class="message-col">					
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/facility_content_img2.jpg" alt="facility" />
					</div><!-- ./image -->      
					<div class="text text-center">
						精密製図サンプルカットシステム
					</div><!-- ./text -->          
				</div><!-- end message-col -->				         
			</div><!-- end message-row -->
			
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col">					
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/facility_content_img3.jpg" alt="facility" />
					</div><!-- ./image -->       
					<div class="text text-center">
						断裁機
					</div><!-- ./text -->       
				</div><!-- end message-col -->
				<div class="message-col">					
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/facility_content_img4.jpg" alt="facility" />
					</div><!-- ./image -->   
					<div class="text text-center">
						精密製図サンプルカットシステム
					</div><!-- ./text -->            
				</div><!-- end message-col -->				         
			</div><!-- end message-row -->			
		</div><!-- end message-group -->
	</div><!-- end primary-row -->	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<table class="facility-content-table">
			<tr>
				<th>用　途</th>
				<th>機械名称</th>
				<th class="facility-price">台　数</th>
			</tr>
			<tr>
				<th>抜型製作</th>
				<td>
					抜型用刃物自動曲げシステム<br />
					抜型用刃物自動切断システム
				</td>
				<td class="facility-price">
					2台<br />
					1台
				</td>
			</tr>
			<tr>
				<th>図面作成</th>
				<td>
					CAD-MARK Vシステム<br />
					精密製図サンプルカットシステム
				</td>
				<td class="facility-price">
					5台<br />
					2台
				</td>
			</tr>
			<tr>
				<th>レーザー加工</th>
				<td>
					レーザーカット機<br />	
					断裁機<br />
					研究用トムソン機(ハイデル・プラテン)

				</td>
				<td class="facility-price">
					2台<br />
					1台<br />
					2台
				</td>
			</tr>
		</table><!-- ./facility-content-table -->
	</div><!-- end primary-row -->	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="facility-btn">
			<a href="<?php bloginfo('url'); ?>/factory">					
				<img src="<?php bloginfo('template_url'); ?>/img/content/facility_content_btn.jpg" alt="facility" />
			</a>
		</div><!-- ./facility-btn -->
	</div><!-- end primary-row -->	
	
<?php get_template_part('part','contact');?>
<?php get_footer(); ?>