<?php get_header(); ?>	
	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<div class="sample-content-info1 clearfix">			
			<p>
				コスト削減、サンプル試作品の制作、形状、色味、デザインなど<br />
				抜型作成前のコンサルティングをご提案します！
			</p>
		</div><!-- ./sample-content-info1 -->
		
		<div class="sample-content-info2-bg clearfix"><!-- begin sample-content-info2 -->
			<div class="sample-content-info2 clearfix">			
				<div class="sample-info2-text">
					<h2 class="sample-info2-title">何か新しいパッケージ・ディスプレイを試してみたい…</h2>
					<p>設備投資には莫大なコストがかかります。特に、パッケージ・ディスプレイは多様に進化し続けているため、自社で新しく設備を持ち製造することは大きなリスクが伴います。</p>
					<p>抜型製作.comは時代やメーカー様のご要望に応えられる、様々な設備を整え、そのようなリスクを全てお引き受けいたします。</p>
					
					<h2 class="sample-info2-title sample-space">現在のコストをもう少し引き下げたい…</h2>
					<p>包装費用のコスト削減もご相談ください！</p>
					<p>抜型製作.comは業界問わず、創業２０年以来の実績がございます。</p>
					<p>その知識と経験を生かし、企画段階から経費の見直しをご提案します！</p>					
				</div><!-- ./sample-info2-text -->
				<div class="sample-info2-img">
					<img src="<?php bloginfo('template_url'); ?>/img/content/sample_content_img1.png" alt="sample" />
				</div><!-- ./sample-info2-img -->
			</div><!-- ./sample-contetn-info2 -->			
		</div><!-- end sample-content-info2 -->				
	</div><!-- end primary-row -->	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->	
		<div class="sample-content-info3 clearfix">
			<img alt="sample" src="<?php bloginfo('template_url');?>/img/content/sample_content_img2.png" />
			<div class="sample-info3-btn">  
				<a href="<?php bloginfo('url'); ?>/sample">
					<img alt="sample" src="<?php bloginfo('template_url');?>/img/content/sample_content_btn.jpg" />
				</a>
			</div><!-- ./sample-info3-btn -->	
		</div><!-- ./sample-content-info -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->	
		<h2 class="h2-title">サンプルカットの実例紹介</h2>
		<h3 class="h3-title">抜型・量産前の試作品に最適</h3>
		<div class="message-right message-375 clearfix"><!-- begin message-375 -->			
			<div class="text">
				<p>抜型製作.comでは抜型製造の他、サンプル機による試作品の作成を行っております。</p>
				<p>抜型を作る前に、まずサンプルカット(有料)をしてほしい、など、お客様のご要望にお応えします。</p>
				<p>塩ピから紙製品、段ボールまでカット出来ますので、一度お気軽にご相談ください。</p>
			</div><!-- ./text -->
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/sample_content_img3.jpg" alt="sample" />
			</div><!-- ./image -->
		</div><!-- end message-375 -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<h3 class="h3-title">確認用やプレゼンテーション用に効果絶大!!</h3>
		<div class="message-right message-375 clearfix"><!-- begin message-375 -->			
			<div class="text">
				<p>CADデータをもとにサンプルカットが可能です。</p>
				<p>あらゆるニーズに対応したサンプルカットが可能です。実際に抜いた製品と同様のものをご提出できます。</p>
			</div><!-- ./text -->
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/sample_content_img4.jpg" alt="sample" />
			</div><!-- ./image -->
		</div><!-- end message-375 -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<h3 class="h3-title">UV印刷を使用して看板や各種缶などの特殊な素材への印刷も対応!!</h3>
		<div class="message-right message-375 clearfix"><!-- begin message-375 -->			
			<div class="text">
				<p>UV印刷とは、紫外線(Ultra Violet ray)に速乾性のあるUVインキを使用し、UV(紫外線)を照射することで、インキを強制的に硬化(乾燥)させる印刷手法です。</p>
				<p>UV印刷により看板・各種缶・ポスターなど通常の印刷では刷ることが難しい特殊な素材への印刷も可能です。</p>
			</div><!-- ./text -->
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/sample_content_img5.jpg" alt="sample" />
			</div><!-- ./image -->
		</div><!-- end message-375 -->
	</div><!-- end primary-row -->		
		
	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<h3 class="h3-title">ミーリング加工を使用しての試作品の製作も承ります!!</h3>
		<div class="message-right message-375 clearfix"><!-- begin message-375 -->			
			<div class="text">
				<p>ミーリングは主に樹脂の切断や溝堀加工です。</p>
				<p>百貨店の化粧品売り場の展示台や看板に使われています。</p>
				<p>回転工具（フライスカッター・エンドミル等）により対象物の平面・曲面部を加工します。</p>
			</div><!-- ./text -->
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/sample_content_img6.jpg" alt="sample" />
			</div><!-- ./image -->
		</div><!-- end message-375 -->
	</div><!-- end primary-row -->	
	
	<?php get_template_part('part','process'); ?>	
	
<?php get_template_part('part','contact');?>
<?php get_footer(); ?>