<?php get_header(); ?>	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2-title">自社工場のご案内</h2> 		
		<p><img src="<?php bloginfo('template_url'); ?>/img/content/factory_content_img1.jpg" alt="factory" /></p>
		<div class="factory-listimg clearfix">			
			<div class="factory-listimg-left">
				<p><img src="<?php bloginfo('template_url'); ?>/img/content/factory_content_img2.jpg" alt="factory" /></p>
				<p class="pt10"><img src="<?php bloginfo('template_url'); ?>/img/content/factory_content_img3.jpg" alt="factory" /></p>
			</div>
			<div class="factory-listimg-right">	
				<p><img src="<?php bloginfo('template_url'); ?>/img/content/factory_content_img4.jpg" alt="factory" /></p>
				<p class="pt10"><img src="<?php bloginfo('template_url'); ?>/img/content/factory_content_img5.jpg" alt="factory" /></p>
			</div>
		</div><!-- ./factory-listimg -->	
	</div><!-- end primary-row -->	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">営業部門</h3>
		<div class="message-group message-col375 message-classic"><!-- begin message-group -->
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col">					
					<div class="image">
						<p>
							斬新なアイデアに最適な提案を何時も考えお客様のニー<br />
							ズにお応えし、お役に立てるよう活動しています。
						</p>
					</div><!-- ./image -->    					        
				</div><!-- end message-col -->
				<div class="message-col">					
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/factory_content_img6.jpg" alt="factory" />
					</div><!-- ./image -->      			          
				</div><!-- end message-col -->				         
			</div><!-- end message-row -->									
		</div><!-- end message-group -->
	</div><!-- end primary-row -->	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">図面作成部門</h3>
		<div class="message-group message-col375 message-classic"><!-- begin message-group -->
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col">					
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/factory_content_img7.jpg" alt="factory" />
					</div><!-- ./image -->    					        
				</div><!-- end message-col -->
				<div class="message-col">					
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/factory_content_img8.jpg" alt="factory" />
					</div><!-- ./image -->      			          
				</div><!-- end message-col -->				         
			</div><!-- end message-row -->									
		</div><!-- end message-group -->
		<p class="pt10">専門スタッフによる正確で的確かつスピーディーに対応し、最新の設備で図面作成に日々心掛けています。</p>
	</div><!-- end primary-row -->	

	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">抜型作成部門</h3>
		<div class="factory-movie clearfix">
			<div class="factory-movie-left">
				<img src="<?php bloginfo('template_url'); ?>/img/content/factory_content_img9.jpg" alt="factory" />
			</div><!-- ./factory-movie-left -->
			<div class="factory-movie-right">
				<iframe width="425" height="240" src="//www.youtube.com/embed/XAIaIcxWLFQ" frameborder="0" allowfullscreen></iframe>
			</div><!-- ./factory-movie-right -->
		</div><!-- end factory-movie -->
		<p class="pt10">抜型の品質の安定を絶えず求め、お客様に満足頂ける製品をお届けできる様、目指しています。</p>
	</div><!-- end primary-row -->	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">レーザー加工部門</h3>
		<div class="message-group message-col375 message-classic"><!-- begin message-group -->
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col">					
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/factory_content_img10.jpg" alt="factory" />
					</div><!-- ./image -->    					        
				</div><!-- end message-col -->
				<div class="message-col">					
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/factory_content_img11.jpg" alt="factory" />
					</div><!-- ./image -->      			          
				</div><!-- end message-col -->				         
			</div><!-- end message-row -->									
		</div><!-- end message-group -->
		<p class="pt10">最新の設備導入でお客様からのハイレベルなご要望に応えて製品を保持しています。</p>
	</div><!-- end primary-row -->	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="facility-btn">
			<a href="<?php bloginfo('url'); ?>/facility">					
				<img src="<?php bloginfo('template_url'); ?>/img/content/factory_content_btn.jpg" alt="factory" />
			</a>
		</div><!-- ./facility-btn -->
	</div><!-- end primary-row -->	
	
<?php get_template_part('part','contact');?>
<?php get_footer(); ?>