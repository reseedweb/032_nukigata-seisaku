<?php get_template_part('header'); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
	<?php if(have_posts()): while(have_posts()) : the_post(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<h2 class="h2-title">自動車部品製造用のハーフカット用木型(トムソン型)</h2>
		<div class="work-detail-content"><!-- begin work-detail-content -->
			<ul class="bxslider">
				<li>
					<?php if( get_field('image1') ): ?>
						<img src="<?php the_field('image1'); ?>" />
					<?php else :?>
						<img src="<?php bloginfo('template_url'); ?>/img/content/work_detail_content_img.jpg" alt="work" />					
					<?php endif; ?>
				</li>
				<li>
					<?php if( get_field('image2') ): ?>
						<img src="<?php the_field('image2'); ?>" />
					<?php else :?>
						<img src="<?php bloginfo('template_url'); ?>/img/content/work_detail_content_img.jpg" alt="work" />					
					<?php endif; ?>
				</li>
				<li>
					<?php if( get_field('image3') ): ?>
						<img src="<?php the_field('image3'); ?>" />
					<?php else :?>
						<img src="<?php bloginfo('template_url'); ?>/img/content/work_detail_content_img.jpg" alt="work" />					
					<?php endif; ?>
				</li>
				<li>
					<?php if( get_field('image4') ): ?>
						<img src="<?php the_field('image4'); ?>" />
					<?php else :?>
						<img src="<?php bloginfo('template_url'); ?>/img/content/work_detail_content_img.jpg" alt="work" />					
					<?php endif; ?>
				</li>
			</ul>
	
			<div id="bx-pager">	
				<ul>	
					<li>
						<a data-slide-index="0" href="#">
							<?php if( get_field('image1') ): ?>
								<img src="<?php the_field('image1'); ?>" />
							<?php else :?>
								<img src="<?php bloginfo('template_url'); ?>/img/content/work_detail_content_img.jpg" alt="work" />					
							<?php endif; ?>
						</a>
					</li>
					<li>
						<a data-slide-index="1" href="#">
							<?php if( get_field('image2') ): ?>
								<img src="<?php the_field('image2'); ?>" />
							<?php else :?>
								<img src="<?php bloginfo('template_url'); ?>/img/content/work_detail_content_img.jpg" alt="work" />					
							<?php endif; ?>
						</a>
					</li>
					<li>
						<a data-slide-index="2" href="#">
							<?php if( get_field('image3') ): ?>
								<img src="<?php the_field('image3'); ?>" />
							<?php else :?>
								<img src="<?php bloginfo('template_url'); ?>/img/content/work_detail_content_img.jpg" alt="work" />					
							<?php endif; ?>
						</a>
					</li>
					<li>
						<a data-slide-index="3" href="#">
							<?php if( get_field('image4') ): ?>
								<img src="<?php the_field('image4'); ?>" />
							<?php else :?>
								<img src="<?php bloginfo('template_url'); ?>/img/content/work_detail_content_img.jpg" alt="work" />					
							<?php endif; ?>
						</a>
					</li>
				</ul>			
			</div><!-- end #bx-pager -->		
		</div> <!-- end work-detail-content --> 
		
		<div class="work-detail-textarea">
			<?php the_content(); ?>
		</div><!-- end work-detail-textarea -->
		
		<div class="work-detail-text">
			<span class="work-detail-ttitle1">業 界</span>&nbsp;&nbsp;<?php echo get_field('material'); ?>
			<span class="work-detail-ttitle2">材 質</span>&nbsp;&nbsp;<?php echo get_field('industry'); ?>
		</div><!-- end work-detail-text -->		
	</div><!-- end primary-row -->    
	
	<div class="primary-row clearfix">	    
		<?php if( get_previous_post() ): ?>	
		<div class="work-detail-bg">
			<div class="work-detail-btn">
				<?php previous_post_link('%link', '« %title'); ?>
			</div>
		</div><!-- ./work-detail-bg -->			
		<?php endif;?>		
	</div><!-- end primary-row -->
	
	<?php endwhile;endif; ?>
</div><!-- end primary-row -->
<div class="primary-row clearfix"><!-- begin primary-row -->        
    <?php get_template_part('part','flow'); ?>
    <?php get_template_part('part','contact'); ?>
</div><!-- end primary-row -->
<?php get_template_part('footer'); ?>