<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
	<div class="side-con clearfix"><!-- begin side-con -->
        <img alt="contact" src="<?php bloginfo('template_url');?>/img/common/side_con_bg.png" />
        <div class="side-con-btn">
            <a href="<?php bloginfo('url'); ?>/contact">
                <img alt="contact" src="<?php bloginfo('template_url');?>/img/common/side_con_btn.jpg" />
            </a>
        </div><!-- ./side-con-btn -->		
    </div><!-- end side-con -->
</div><!-- end sidebar-row -->

<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
	<div class="side-sample clearfix"><!-- begin side-con -->
        <img alt="sample bg" src="<?php bloginfo('template_url');?>/img/common/side_sample_bg.png" />
		<div class="side-sample-text">
			<ul>
				<li>
					抜型は、まだ必要ないけど<br />
					<span class="side-sclr">試作品が欲しい</span>
				</li>
				<li><span class="side-sclr">サンプルの製作</span>がしたい</li>
				<li><span class="side-sclr">小ロット</span>での製作をお願いしたい</li>
			</ul>
		</div>
        <div class="side-sample-btn">
            <a href="<?php bloginfo('url'); ?>/sample">
                <img alt="sample" src="<?php bloginfo('template_url');?>/img/common/side_sample_btn.jpg" />
            </a>
        </div><!-- ./side-con-btn -->		
    </div><!-- end side-con -->
</div><!-- end sidebar-row -->

<?php get_template_part('part','snavi'); ?>

<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
	<div class="side-work">
            <a href="<?php bloginfo('url'); ?>/work">
                <img alt="work" src="<?php bloginfo('template_url');?>/img/common/side_work.png" />
            </a>
    </div><!-- ./side-work -->	
</div><!-- end sidebar-row -->

<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
	<div class="side-staff">
            <a href="<?php bloginfo('url'); ?>/staff">
                <img alt="staff" src="<?php bloginfo('template_url');?>/img/common/side_staff.png" />
            </a>
    </div><!-- ./side-staff -->	
</div><!-- end sidebar-row -->


<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
	<div class="side-factory">
            <a href="<?php bloginfo('url'); ?>/factory">
                <img alt="factory" src="<?php bloginfo('template_url');?>/img/common/side_factory.png" />
            </a>
    </div><!-- ./side-factory -->	
</div><!-- end sidebar-row -->

<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
	<p>
		<a href="<?php bloginfo('url'); ?>/blog">
			<img alt="blog" src="<?php bloginfo('template_url');?>/img/common/side_blog.jpg" />
        </a>
	</p>	
</div><!-- end sidebar-row -->
