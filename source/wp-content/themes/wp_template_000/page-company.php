<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2-title">会社概要</h2>
		<table class="company-table"><!-- begin company-table -->
			<tr>
				<th>会社名</th>
				<td>有限会社ケイエスシステム</td>
			</tr>
			<tr>
				<th>設　立</th>
				<td>33259</td>
			</tr>
			<tr>
				<th>資本金</th>
				<td>300万円</td>
			</tr>
			<tr>
				<th>代 表 者</th>
				<td>代表取締役　片岡孝治</td>
			</tr>
			<tr>
				<th>本社所在地</th>
				<td>大阪府東大阪市荒本北１９９番地</td>
			</tr>
			<tr>
				<th>TEL</th>
				<td>06-6744-3907</td>
			</tr>
			<tr>
				<th>FAX</th>
				<td>06-6744-4522</td>
			</tr>
			<tr>
				<th>取引金融機関</th>
				<td>
					大阪市信用金庫高井田支店、<br />
					池田銀行東大阪支店
				</td>
			</tr>
			<tr>
				<th>営業品目</th>
				<td>各種抜型、パッケージディスプレイ各種サンプル、レーザーカッティング</td>
			</tr>					
		</table><!-- ./end company-table -->
	</div><!-- end primary-row -->	


	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2-title">アクセス</h2>
		<div id="company_map">
			<iframe width="760" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=vi&amp;geocode=&amp;q=%E5%A4%A7%E9%98%AA%E5%BA%9C%E6%9D%B1%E5%A4%A7%E9%98%AA%E5%B8%82%E8%8D%92%E6%9C%AC%E5%8C%97%EF%BC%91%EF%BC%99%EF%BC%99&amp;aq=&amp;sll=34.683427,135.597227&amp;sspn=0.011981,0.024397&amp;gl=VN&amp;ie=UTF8&amp;hq=&amp;hnear=Aramotokita,+Higashiosaka,+Osaka+Prefecture+577-0011,+Nh%E1%BA%ADt+B%E1%BA%A3n&amp;t=m&amp;z=14&amp;ll=34.683427,135.597227&amp;output=embed"></iframe>
		</div><!-- ./company_map -->
	</div><!-- ./primary-row -->
	<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>