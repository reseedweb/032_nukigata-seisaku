<?php get_header(); ?>	
	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<div class="reason-content-info1 clearfix">			
			<img src="<?php bloginfo('template_url'); ?>/img/content/reason_content_img1.jpg" alt="reason" />
			<div class="reason-info1-text">
				<p>
					私たち「抜型.com」は、「NOとは言わない抜型・木型づくり！お客様の要望<br />
					をカタチにします。」を合言葉に 抜型製作を追求しております。
				</p>
				<p>
					「この様な形は可能だろうか？」「特殊なデータで依頼したい。」「コスト低減を<br />
					したい」といったお客様の声にお応えしたい。そういった想いから、当社は自<br />
					社で生産できる体制を整えているとともに、長年の経験から培ってきた 知識・<br />
					技術に裏打ちされたサポート体制も整えております。
				</p>
			</div><!-- ./reason-info1-text -->			
		</div><!-- ./reason-content-info1 -->
		
		<div class="reason-title-bg reason-space clearfix"><!-- begin reason-title-bg -->
			<img src="<?php bloginfo('template_url'); ?>/img/content/reason_content_step1.png" alt="reason" />
			<h2 class="reason-title">
				<span class="reason-title-clr">最短2日～</span>全国対応で納品可能
			</h2>
		</div><!-- end reason-title-bg -->
		
		<div class="message-right message-reason375 clearfix"><!-- begin message-reason375 -->			
			<div class="text">
				<p>当社は創業20年の経験から培った確かな技術をもとに、高品質の抜型・木型を製作します。</p>
				<p>これまで、品質に関するクレームはほぼゼロで、リピート率91%という高さを保っております。</p>
				<p>また、短納期に関しては社内ネットワークを整備することで納品までの時間を大幅カットしています。</p>
			</div><!-- ./text -->
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/reason_content_img2.jpg" alt="reason" />
			</div><!-- ./image -->
		</div><!-- end message-reason375 -->
	</div><!-- end primary-row -->	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<div class="reason-title-bg clearfix"><!-- begin reason-title-bg -->
			<img src="<?php bloginfo('template_url'); ?>/img/content/reason_content_step2.png" alt="reason" />
			<h2 class="reason-title">
				多様な機械を所有しているので<span class="reason-title-clr">どんな抜型でも対応可</span>
			</h2>
		</div><!-- end reason-title-bg -->
		
		<div class="message-right message-reason375 clearfix"><!-- begin message-reason375 -->			
			<div class="text">
				<p>トムソン型(オートン・ハイデル・ビク・アップダウン)の抜型はもちろん、小ロット・試作用途のサンプルカット、ベニヤ材・アクリルやベイク-PP-ABSなどの樹脂を加工するレーザーカットを行う設備も所有している為、多様な要望にお応えすることが可能です。</p>
				<p>また、小ロットからの作成も行っており確認用やプレゼンテーション用に効果絶大な試作品も製作することが可能です。</p>
			</div><!-- ./text -->
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/reason_content_img3.jpg" alt="reason" />
			</div><!-- ./image -->
		</div><!-- end message-reason375 -->
	</div><!-- end primary-row -->	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<div class="reason-title-bg clearfix"><!-- begin reason-title-bg -->
			<img src="<?php bloginfo('template_url'); ?>/img/content/reason_content_step3.png" alt="reason" />
			<h2 class="reason-title">
				<span class="reason-title-clr">100,000万版以上</span>の製作実績!
			</h2>
		</div><!-- end reason-title-bg -->
		
		<div class="message-right message-reason375 clearfix"><!-- begin message-reason375 -->			
			<div class="text">
				<p>抜型・木型の事なら、抜型製作.comにお任せください!!創業以来、トムソン抜型を、創意工夫を重ねて作ってまいりました。</p>
				<p>その成果もあり、様々な業界で100,000万版以上の抜型を製作してきました。</p>
				<p>業界問わず抜型・木型を作成できることが一番の当社の強みです。</p>
				<p>また、お客様からの質問や相談に対する素早いレスポンスで、多くのご要望にお応えしてまいりました。</p>
			</div><!-- ./text -->
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/reason_content_img4.jpg" alt="reason" />
			</div><!-- ./image -->
		</div><!-- end message-reason375 -->
	</div><!-- end primary-row -->	
		
	<div class="primary-row clearfix"><!-- begin primary-row -->			
		<div class="reason-title-bg clearfix"><!-- begin reason-title-bg -->
			<img src="<?php bloginfo('template_url'); ?>/img/content/reason_content_step4.png" alt="reason" />
			<h2 class="reason-title">
				<span class="reason-title-clr">低価格、短納期</span>などのニーズにあった要望にお応えします!!
			</h2>
		</div><!-- end reason-title-bg -->
		
		<div class="message-right message-reason375 clearfix"><!-- begin message-reason375 -->			
			<div class="text">
				<p>当社は、開発力と技術力に裏打ちされたソリューションをお客様に提供し、お客様のご要望にお応えしています。</p>
				<p>精通した営業スタッフと技術スタッフによりお客様固有のご要望に応えるべく活動をしています。価格・納期など、お客様から頂いた要望に柔軟に対応確固たる信頼関係を築いています。</p></p>
			</div><!-- ./text -->
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/reason_content_img5.jpg" alt="reason" />
			</div><!-- ./image -->
		</div><!-- end message-reason375 -->	
	</div><!-- end primary-row -->	
		
<?php get_template_part('part','contact');?>
<?php get_footer(); ?>