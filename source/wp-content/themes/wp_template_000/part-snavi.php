<div class="sidebar-row clearfix">
	<aside id="sideNavi"><!-- begin sideNavi -->		
		<ul class="sideNavi-content">
			<li><a href="<?php bloginfo('url'); ?>/">ホーム</a></li>
			<li><a href="<?php bloginfo('url'); ?>/about">抜型について</a></li>
			<li><a href="<?php bloginfo('url'); ?>/laser">レーザーカット</a></li>
			<li><a href="<?php bloginfo('url'); ?>/reason">選ばれる理由</a></li>
			<li><a href="<?php bloginfo('url'); ?>/work">製作事例</a></li>
			<li><a href="<?php bloginfo('url'); ?>/factory">工場紹介</a></li>
			<li><a href="<?php bloginfo('url'); ?>/contact">お問い合わせ</a></li>
			<li><a href="<?php bloginfo('url'); ?>/estimate">お見積もり</a></li>
			<li><a href="<?php bloginfo('url'); ?>/sample">サンプルカット</a></li>
			<li><a href="<?php bloginfo('url'); ?>/blog">ブログ</a></li>
			<li><a href="<?php bloginfo('url'); ?>/flow">納品までの流れ</a></li>
			<li><a href="<?php bloginfo('url'); ?>/staff">スタッフ紹介</a></li>
			<li><a href="<?php bloginfo('url'); ?>/faq">よくあるご質問</a></li>
			<li><a href="<?php bloginfo('url'); ?>/facility">設備紹介</a></li>
			<li><a href="<?php bloginfo('url'); ?>/company">運営会社</a></li>
			<li><a href="<?php bloginfo('url'); ?>/privacy">プライバシーポリシー</a></li>
		</ul>
	</aside><!-- end sideNavi -->
</div>
                       