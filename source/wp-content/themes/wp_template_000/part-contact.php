<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2-title">納品までの流れ</h2>
	<p>
		<a href="<?php bloginfo('url'); ?>/flow">
			<img alt="top" src="<?php bloginfo('template_url');?>/img/content/top_content_flow.jpg" />
		</a>
	</p>
	<div class="primary-row clearfix">
		<div class="top-content-info6 clearfix">
			<img alt="top" src="<?php bloginfo('template_url');?>/img/content/top_content_contact.png" />
			<div class="top-info6-btn">
				<a href="<?php bloginfo('url'); ?>/contact">
					<img alt="top" src="<?php bloginfo('template_url');?>/img/content/top_contact_btn.jpg" />
				</a>
			</div><!-- ./top-info6 -->	
		</div><!-- ./top-content-info5 -->
	</div><!-- ./primary-row -->
</div><!-- end primary-row -->