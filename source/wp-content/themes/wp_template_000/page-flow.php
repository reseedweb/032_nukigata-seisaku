<?php get_header(); ?>	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2-title">お問い合わせから納品までの流れをご紹介</h2>  
		<div class="flow-content-bg clearfix">
			<div class="flow-content clearfix">
				<div class="flow-step">
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_step1.jpg" alt="flow" /></p>
				</div><!-- ./flow-step -->
				<div class="flow-info">
					<h3 class="flow-title">お問い合わせ・ヒアリング</h3>
					<p>まずは、お電話・メールにて弊社までご連絡ください。</p>
					<p>ちょっとしたご質問、ご相談でも構いません。専門のスタッフが対応させていただきます。</p>
					<p>CADデータがあれば、お見積もり・加工も早く行うことが可能です。</p>			
					<div class="flow-tel">
						<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img1.jpg" alt="flow" />
					</div><!-- ./flow-tel-->
					<div class="flow-btn">
						<a href="<?php bloginfo('url'); ?>/contact">					
							<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_btn.jpg" alt="flow" />
						</a>
					</div><!-- ./flow-btn-->
				</div><!-- ./flow-info -->
			</div><!-- ./flow-content -->
		</div><!-- end flow-content-bg  -->
		
		<div class="flow-arrow clearfix">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.png" alt="flow" />
		</div><!-- end flow-arrow -->
		
		<div class="flow-content-bg clearfix">
			<div class="flow-content clearfix">
				<div class="flow-step">
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_step2.jpg" alt="flow" /></p>
				</div><!-- ./flow-step -->
				<div class="flow-info">
					<h3 class="flow-title">社内打ち合わせ</h3>
					<div class="flow-text">
						<p>お客様のニーズにお応えするため、綿密なお打ち合わせを行います。</p>
						<p class="pt20">打ち合わせ後、見積もり・納期をご提示しますので、ご納得頂けましたら発注をお願いします。</p>					
					</div><!-- ./flow-text -->				
					<div class="flow-img">
						<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img2.jpg" alt="flow" />
					</div><!-- ./flow-tel-->					
				</div><!-- ./flow-info -->
			</div><!-- ./flow-content -->
		</div><!-- end flow-content-bg  -->
		
		<div class="flow-arrow clearfix">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.png" alt="flow" />
		</div><!-- end flow-arrow -->
		
		<div class="flow-content-bg clearfix">
			<div class="flow-content clearfix">
				<div class="flow-step">
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_step3.jpg" alt="flow" /></p>
				</div><!-- ./flow-step -->
				<div class="flow-info">
					<h3 class="flow-title">CADによるイメージ作成</h3>
					<div class="flow-text">
						<p>お客様とお打合せの内容をもとに、CADでイメージを作成します。</p>						
					</div><!-- ./flow-text -->				
					<div class="flow-img">
						<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img3.jpg" alt="flow" />
					</div><!-- ./flow-tel-->					
				</div><!-- ./flow-info -->
			</div><!-- ./flow-content -->
		</div><!-- end flow-content-bg  -->
		
		<div class="flow-arrow clearfix">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.png" alt="flow" />
		</div><!-- end flow-arrow -->
		
		<div class="flow-content-bg clearfix">
			<div class="flow-content clearfix">
				<div class="flow-step">
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_step4.jpg" alt="flow" /></p>
				</div><!-- ./flow-step -->
				<div class="flow-info">
					<h3 class="flow-title">抜型作成</h3>
					<div class="flow-text">
						<p>職人が手作業による抜型の制作とCAD/CAMによるデータを入力する場合に分かれます。</p>
						<p>機械による制作でもこまかい補正などは、やはり職人たちが一つ一つ丁寧に作業を行っていきます。</p>						
					</div><!-- ./flow-text -->				
					<div class="flow-img">
						<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img4.jpg" alt="flow" />
					</div><!-- ./flow-tel-->					
				</div><!-- ./flow-info -->
			</div><!-- ./flow-content -->
		</div><!-- end flow-content-bg  -->
		
		<div class="flow-arrow clearfix">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.png" alt="flow" />
		</div><!-- end flow-arrow -->
		
		<div class="flow-content-bg clearfix">
			<div class="flow-content clearfix">
				<div class="flow-step">
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_step5.jpg" alt="flow" /></p>
				</div><!-- ./flow-step -->
				<div class="flow-info">
					<h3 class="flow-title">最終チェックシステム</h3>
					<div class="flow-text">
						<p>抜型の品質には自信を持っていても、目に見えないほどの誤差や実際に商品を「試抜き」してみないことには完成とはいえません。</p>										
					</div><!-- ./flow-text -->				
					<div class="flow-img">
						<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img5.jpg" alt="flow" />
					</div><!-- ./flow-tel-->					
				</div><!-- ./flow-info -->
			</div><!-- ./flow-content -->
		</div><!-- end flow-content-bg  -->
		
		<div class="flow-arrow clearfix">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.png" alt="flow" />
		</div><!-- end flow-arrow -->
		
		<div class="flow-content-bg clearfix">
			<div class="flow-content clearfix">
				<div class="flow-step">
					<p><img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_step6.jpg" alt="flow" /></p>
				</div><!-- ./flow-step -->
				<div class="flow-info">
					<h3 class="flow-title">納 品</h3>
					<div class="flow-text">
						<p>最終チェックが終わってから、お客様の元へ製品をお届けします。</p>										
					</div><!-- ./flow-text -->				
					<div class="flow-img">
						<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img6.jpg" alt="flow" />
					</div><!-- ./flow-tel-->					
				</div><!-- ./flow-info -->
			</div><!-- ./flow-content -->
		</div><!-- end flow-content-bg  -->
	</div><!-- end primary-row -->	
	
<?php get_template_part('part','contact');?>
<?php get_footer(); ?>