 <?php get_header(); ?>	
	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<h2 class="h2-title">抜型とは</h2>
		<div class="message-right message-375 clearfix"><!-- begin message-375 -->			
			<div class="text">
				<p>抜型(木型)とは、私たちの生活の中でとても身近なものに作るのに使われています。</p>
				<p>例えば、ティッシュペーパーの箱、お菓子の箱、医薬品の箱、鍋の箱、電化製品の箱、などetc.</p>
				<p>上記の例のような、私たちが普段何気なく使っている、所謂パッケージは抜型を使用されています。</p>
				<p class="pt20">抜型は、ベニヤ板に製品の展開図どおりに溝を切り、刃や罫（折目をつける為の刃）を組み込んだものを指します。</p>
			</div><!-- ./text -->
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/about_content_img1.jpg" alt="about" />
			</div><!-- ./image -->
		</div><!-- end message-375 -->			
	</div><!-- end primary-row -->	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<h2 class="h2-title">抜型はどのように使用するのか</h2>
		<div class="message-right message-375 clearfix"><!-- begin message-375 -->			
			<div class="text">
				<p>抜型は打抜加工に使われます。</p>
				<p>ここでいう打抜加工とは、主にシート状のものを抜型を使って形に抜く加工のことです。</p>
				<p>とても単純な加工のようですが、材質・形・環境によっていろいろなトラブルが発生します。</p>
				<p>抜型製作.comでは50年以打抜加工一筋に取り組んできて、抜型(木型)を提供してまいりました。</p>
				<p class="pt20">抜型(木型)にはトムソン型(オートン・ハイデル・ビク・アップダウン)、ピナクル型（腐食刃）、彫刻型と種類がありますが、抜型製作.comでは、主にトムソン型(オートン・ハイデル・ビク・アップダウン)をメインで使用してきました。</p>
			</div><!-- ./text -->
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/about_content_img2.jpg" alt="about" />
			</div><!-- ./image -->
		</div><!-- end message-375 -->			
	</div><!-- end primary-row -->
	
	<?php get_template_part('part','benefit'); ?>		
	<?php get_template_part('part','process'); ?>	
	<?php get_template_part('part','work');?>	
	<?php get_template_part('part','contact');?>
<?php get_footer(); ?>