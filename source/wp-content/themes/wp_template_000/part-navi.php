<nav><!-- begin nav -->
    <div id="gNavi" class="wrapper-header">
        <ul class="dynamic clearfix">
			<li><a href="<?php bloginfo('url'); ?>/">ホーム</a></li>                           
			<li><a href="<?php bloginfo('url'); ?>/about">抜型について</a></li>
            <li><a href="<?php bloginfo('url'); ?>/laser">レーザーカット</a></li>
			<li><a href="<?php bloginfo('url'); ?>/reason">選ばれる理由</a></li>
			<li><a href="<?php bloginfo('url'); ?>/work">製作事例</a></li>
			<li><a href="<?php bloginfo('url'); ?>/factory">工場紹介</a></li>
			<li><a href="<?php bloginfo('url'); ?>/contact">お問い合わせ</a></li>
			<li><a href="<?php bloginfo('url'); ?>/estimate">お見積もり</a></li>
		</ul><!-- ./dynamic -->
	</div><!-- ./gNavi -->
</nav><!-- end nav -->
                       