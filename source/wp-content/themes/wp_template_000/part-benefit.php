	<div class="primary-row clearfix"><!-- begin primary-row -->	
		<h2 class="h2-title">レーザーカットのメリット</h2>
		<div class="benefit-laser-bg clearfix"><!-- begin benefit-laser-bg -->
			<img src="<?php bloginfo('template_url'); ?>/img/content/benefit_laser_img1.png" alt="benefit" />
			<h2 class="benefit-ltitle">複雑で細かなデザインをカットできる</h2>
		</div><!-- end benefit-laser-bg-->
		
		<div class="benefit-laser-bg clearfix"><!-- begin benefit-laser-bg -->
			<img src="<?php bloginfo('template_url'); ?>/img/content/benefit_laser_img2.png" alt="benefit" />
			<h2 class="benefit-ltitle">型を使わないため小ロットも得意。</h2>
		</div><!-- end benefit-laser-bg-->			
		
		<div class="benefit-laser-bg clearfix"><!-- begin benefit-laser-bg -->
			<img src="<?php bloginfo('template_url'); ?>/img/content/benefit_laser_img3.png" alt="benefit" />
			<h2 class="benefit-ltitle">薄い材料でも安心</h2>
		</div><!-- end benefit-laser-bg-->	
		
		<div class="benefit-laser-bg clearfix"><!-- begin benefit-laser-bg -->
			<img src="<?php bloginfo('template_url'); ?>/img/content/benefit_laser_img4.png" alt="benefit" />
			<h2 class="benefit-ltitle">周りだけでなく、複数の箇所にカットも可能。</h2>
		</div><!-- end benefit-laser-bg-->	
		
		<div class="benefit-laser-bg clearfix"><!-- begin benefit-laser-bg -->
			<img src="<?php bloginfo('template_url'); ?>/img/content/benefit_laser_img5.png" alt="benefit" />
			<h2 class="benefit-ltitle">型が不要</h2>
		</div><!-- end benefit-laser-bg-->	
	</div><!-- end primary-row -->