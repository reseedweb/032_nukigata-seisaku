
                    </main><!-- end primary -->
                    <aside class="sidebar"><!-- begin sidebar -->
                        <?php if(is_page('blog') || is_category() || is_single()) : ?>
                            <?php
                            $queried_object = get_queried_object();                                
                            $sidebar_part = 'blog';
                            if(is_tax() || is_archive()){                                    
                                $sidebar_part = '';
                            }                               

                            if($queried_object->post_type != 'post' && $queried_object->post_type != 'page'){
                                $sidebar_part = '';
                            }   
                                    
                            if($queried_object->taxonomy == 'category'){                                    
                                $sidebar_part = 'blog';
                            }                 
                            ?>
                            <?php get_template_part('sidebar',$sidebar_part); ?>  
                        <?php else: ?>
                            <?php get_template_part('sidebar'); ?>  
                        <?php endif; ?>                                    
                    </aside>                            
                </div><!-- end wrapper -->            
            </section><!-- end content -->

            <footer><!-- begin footer -->
                <div class="footer-content clearfix"><!-- begin footer-content -->                    
					<div class="wrapper-header clearfix"><!-- begin wrapper -->
						<div class="footer-col1">
							<p>
								<a href="<?php bloginfo('url'); ?>/">
									<img src="<?php bloginfo('template_url'); ?>/img/common/footer_logo.jpg" />
								</a>
							</p>
							<p class="pt20 pb20">
								有限会社ケイエスシステム<br />
								大阪府東大阪市荒本北3丁目4番5号
							</p>
							<p>
								<a href="<?php bloginfo('url'); ?>/contact">
									<img src="<?php bloginfo('template_url'); ?>/img/common/footer_con.jpg" />
								</a>
							</p>
						</div><!-- end footer-col1 -->	
						
						<div class="footer-col2">
							<p><img src="<?php bloginfo('template_url'); ?>/img/common/footer_tel.jpg" /></p>							
						</div><!-- end footer-col2 -->	
						
						<div class="footer-col3">
							<ul>
								<li>
									<a href="<?php bloginfo('url'); ?>/">ホーム</a>									
								</li>
								<li>
									<a href="<?php bloginfo('url'); ?>/about">抜型について</a>									
								</li>
								<li>
									<a href="<?php bloginfo('url'); ?>/laser">レーザーカット</a>									
								</li>								
								<li>
									<a href="<?php bloginfo('url'); ?>/reason">選ばれる理由</a>									
								</li>	
								<li>
									<a href="<?php bloginfo('url'); ?>/work">製作事例</a>									
								</li>										
							</ul>
							
							<ul>
								<li>
									<a href="<?php bloginfo('url'); ?>/factory">工場紹介</a>									
								</li>
								<li>
									<a href="<?php bloginfo('url'); ?>/contact">お問い合わせ</a>									
								</li>
								<li>
									<a href="<?php bloginfo('url'); ?>/estimate">お見積もり</a>									
								</li>	
								<li>
									<a href="<?php bloginfo('url'); ?>/sample">サンプルカット</a>									
								</li>
								<li>
									<a href="<?php bloginfo('url'); ?>/flow">納品までの流れ</a>									
								</li>
							</ul>
							
							<ul>
								<li>
									<a href="<?php bloginfo('url'); ?>/staff">スタッフ紹介</a>									
								</li>
								<li>
									<a href="<?php bloginfo('url'); ?>/faq">よくあるご質問</a>									
								</li>
								<li>
									<a href="<?php bloginfo('url'); ?>/facility">設備紹介</a>									
								</li>	
								<li>
									<a href="<?php bloginfo('url'); ?>/company">運営会社</a>									
								</li>
								<li>
									<a href="<?php bloginfo('url'); ?>/privacy">プライバシーポリシー</a>									
								</li>
							</ul>						
						</div><!-- end footer-col3 -->
						
					</div><!-- end wrapper-header -->
                </div><!-- end footer-content -->
                <div class="footer-copyright clearfix">
                    Copyright © 2015 抜型製作.com All Rights Reserved.
                </div><!-- ./ooter-copyright  -->         
            </footer><!-- end footer -->            

        </div><!-- end wrapper -->
        <?php wp_footer(); ?>
        <!-- js plugins -->
        <script type="text/javascript">

            jQuery(document).ready(function(){
                // dynamic gNavi
                var gNavi_item_count = $('#gNavi .dynamic > li').length;
                if(gNavi_item_count > 0){
                    var gNavi_item_length = $('#gNavi .dynamic').width() / parseInt(gNavi_item_count);                    
                    $('#gNavi .dynamic > li').css('width', Math.floor( gNavi_item_length ) + 'px' )
                }
                $('#gNavi .dynamic > li').hover(function(){
                    $(this).addClass('current');
                },function(){
                    $(this).removeClass('current');
                });
                // bxslider
                jQuery('.bxslider').bxSlider({
                    pagerCustom: '#bx-pager',
                    pause : 3000,
                    auto : true,
                });                
            }); 
        </script>                
    </body>
</html>