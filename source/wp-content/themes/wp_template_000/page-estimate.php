<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->	
		<p>弊社ではメールによるお見積もりもいたしております。</p>
		<p>仕様に関するできるだけ詳しい情報をお書き下さい。</p>
		<p>詳しい内容の確認をさせていただいた上で、お客様に一番喜んでいただけるご提案をさせていただきます。</p>
		<p class="pt20 pb20"><img alt="estimate" src="<?php bloginfo('template_url'); ?>/img/content/estimate_content_contact.jpg" /></p>
		<?php echo do_shortcode('[contact-form-7 id="284" title="お見積り"]') ?>
		<script src="http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js" charset="UTF-8"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$('#zip').change(function(){					
					//AjaxZip3.JSONDATA = "https://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/zipdata";
	    			AjaxZip3.zip2addr(this,'','pref','addr';
	  			});
			});
		</script>					
	</div><!-- end primary-row -->
	<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>