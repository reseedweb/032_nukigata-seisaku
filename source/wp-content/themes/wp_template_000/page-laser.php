<?php get_header(); ?>	
	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<h2 class="h2-title">レーザーカットとは</h2>
		<div class="message-right message-375 clearfix"><!-- begin message-375 -->			
			<div class="text">
				<p>抜型作成(木型)、サンプルカットと合わせ、抜型製作.comのもう一つの売りは、職人による正確なレーザーカットです。</p>
				<p>当社では抜型製造に関係なく、ベニヤ材の他、アクリルやベイク-PP-ABSなどの樹脂もレーザーでカットしております。</p>
				<p>
					「こんなものもレーザーで切れるのかしら?」	<br />
					「こういう商品の形をこの樹脂でカットして欲しいけど…」<br/>
					など、一度お気軽にご相談下さい。
				</p>
				<p>もちろん緊急のためにレーザーカット依頼したい等、全対応お任せ下さい。</p>
				<p>他社からの各種レーザーカットも随時受注致しております。</p>
			</div><!-- ./text -->
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/laser_content_img.jpg" alt="laser" />
			</div><!-- ./image -->
		</div><!-- end message-375 -->			
	</div><!-- end primary-row -->	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->	
		<h2 class="h2-title">トムソン抜型では、出来ない微細な動きも可能！</h2>
		<p>現在の抜き加工で最も繊細な抜きが可能な「レーザーカット」。レーザー光線によって焼き切ることでカットするため、非常に細かく、美麗な抜き加工ができます。円や曲線･点線など抜きの形にもこだわらず自由にカットできるのが魅力です。</p>
	</div><!-- end primary-row -->
	
	<?php get_template_part('part','benefit'); ?>	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->	
		<h2 class="h2-title">レーザーカットで対応出来る用途一例</h2>
		<p>紙だけではなく、レーザー光ではさまざまな素材のカットや彫刻が可能なので切り絵調イラストやロゴサインなどのシンプルなレーザーカットから、レース調の微細カットまで、印刷とは違うレーザー切り抜きならではの高級感が演出できます。</p>
		<p class="pt20">
			・個性的な名刺やダイレクトメール（DM）<br />
			・結婚式などの各種案内状<br />
			・ペーパークラフト<br />
			・ポップアップ（飛び出し・仕掛け）
		</p>
	</div><!-- end primary-row -->	
	
	<?php get_template_part('part','process'); ?>		
	
	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<div class="laser-text"><!-- begin las -->
			<p>レーザーカットのみのご依頼も受け付けております。</p>
			<p>お気軽にご相談ください。</p>
		</div><!-- end laser -->
	</div><!-- end primary-row -->	
	
	<?php get_template_part('part','work');?>	
	<?php get_template_part('part','contact');?>
<?php get_footer(); ?>